package com.knasirayaz.weatherapp.presentation.ui.splash

import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.knasirayaz.common.utils.Constants
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.mohapcovid.view.utils.launchActivity
import com.knasirayaz.weatherapp.base.BaseActivity
import com.knasirayaz.weatherapp.R
import com.knasirayaz.weatherapp.presentation.ui.main.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : BaseActivity(){

    private val activityScope = CoroutineScope(Dispatchers.Main)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: SplashViewModel by viewModels { viewModelFactory }


    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    /**
    Check if its first time start -NO-> Start Home Fragment
     Yes -> check Location Services and Launch SearchFragment
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityScope.launch {
            delay(1000)
            launchActivity<MainActivity>(finishActivity = true)

         /*   if (appPreferences.contains(Constants.IS_FIRST_TIME)) {
                launchActivity<MainActivity>(finishActivity = true)
            }else{
                launchActivity<MainActivity>(finishActivity = true)

            }*/
        }
    }
}