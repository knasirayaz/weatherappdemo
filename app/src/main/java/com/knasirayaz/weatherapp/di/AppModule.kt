package com.knasirayaz.weatherapp.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.weatherapp.MyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    fun provideGson(): Gson =
        GsonBuilder()
            .setLenient()
            .create()

    @Provides
    fun provideAppPreferences(application: Context, gson: Gson) =
        SharedPreferencesImpl(application, gson)



}
