package com.knasirayaz.weatherapp.di

import com.knasirayaz.weatherapp.presentation.ui.main.MainActivity
import com.knasirayaz.weatherapp.presentation.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector()
    abstract fun contributeMainActivity(): MainActivity
}