package com.knasirayaz.weatherapp.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.knasirayaz.common.di.CoreComponent
import com.knasirayaz.common.scopes.AppScope
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.weatherapp.MyApplication
import com.knasirayaz.weatherapp.presentation.ui.main.MainActivity
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Scope
import javax.inject.Singleton

@Component(
    dependencies = [CoreComponent::class],
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class]
)
interface AppComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun coreComponent(coreComponent: CoreComponent): Builder
        fun build(): AppComponent
    }
}