package com.knasirayaz.weatherapp

import android.content.Context
import com.google.android.play.core.splitcompat.SplitCompatApplication
import com.knasirayaz.common.di.ContextModule
import com.knasirayaz.common.di.CoreComponent
import com.knasirayaz.common.di.DaggerCoreComponent
import com.knasirayaz.weatherapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class MyApplication : DaggerApplication(){
    lateinit var coreComponent: CoreComponent

    companion object {
        /**
         * Obtain core dagger component.
         *
         * @param context The application context
         */
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as? MyApplication)?.coreComponent
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        initCoreDependencyInjection()
        return DaggerAppComponent
            .builder()
            .application(this)
            .coreComponent(coreComponent)
            .build()


    }


    /**
     * Initialize core dependency injection component.
     */
    private fun initCoreDependencyInjection() {
        coreComponent = DaggerCoreComponent
            .builder()
            .contextModule(ContextModule(this))
            .build()
    }
}