package com.knasirayaz.weatherapp.base

class BaseResponse {
    var StatusCode : String? = null
    var Message : String? = null
}