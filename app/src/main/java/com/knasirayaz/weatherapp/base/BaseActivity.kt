package com.knasirayaz.weatherapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.knasirayaz.common.utils.SharedPreferencesImpl
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }
}