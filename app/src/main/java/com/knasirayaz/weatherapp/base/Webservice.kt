package com.knasirayaz.weatherapp.base

import okhttp3.RequestBody
import retrofit2.http.*


interface Webservice {

    @Headers("No-Authentication: true")
    @GET("api/Announcement/GetConvidCounters")
    suspend fun getConvidCounters(): BaseResponse


    @Headers("No-Authentication: true")
    @POST("api/Announcement/PostAssessment")
    suspend fun postAssessment(
        @Header("lang") lang: String,
        @Header("lat") lat: Double,
        @Header("lng") lng: Double,
        @Header("devicenumber") devicenumber: String,
        @Header("result") result: String,
        @Body request: RequestBody
    ): BaseResponse


}



