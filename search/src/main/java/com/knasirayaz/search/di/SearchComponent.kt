/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.knasirayaz.search.di

import com.knasirayaz.common.di.CoreComponent
import com.knasirayaz.common.scopes.FeatureScope
import com.knasirayaz.search.presentation.ui.SearchFragment
import com.knasirayaz.search.presentation.ui.ViewCityFragment
import dagger.Component

/**
 * Class for which a fully-formed, dependency-injected implementation is to
 * be generated from [SearchModule].
 *
 * @see Component
 */
@FeatureScope
@Component(
    modules = [ProviderModule::class],
    dependencies = [CoreComponent::class]
)
interface SearchComponent {

    /**
     * Inject dependencies on component.
     *
     * @param searchFragment Home component.
     */
    fun inject(searchFragment: SearchFragment)
    fun inject(searchFragment: ViewCityFragment)

}
