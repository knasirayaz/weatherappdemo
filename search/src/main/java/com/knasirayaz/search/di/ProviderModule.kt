/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.knasirayaz.search.di

import android.content.Context
import androidx.fragment.app.Fragment
import com.dmgdesignuk.locationutils.easylocationutility.EasyLocationUtility
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.knasirayaz.common.BuildConfig
import com.knasirayaz.common.extentions.viewModel
import com.knasirayaz.common.scopes.FeatureScope
import com.knasirayaz.common.utils.AuthorizationInterceptor
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.search.data.local.WeatherDatabase
import com.knasirayaz.search.domain.network.ServiceProtocol
import com.knasirayaz.search.domain.protocols.SearchRepoProtocol
import com.knasirayaz.search.domain.repository.SearchRepoImpl
import com.knasirayaz.search.presentation.ui.SearchFragment
import com.knasirayaz.search.presentation.ui.SearchViewModel
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Class that contributes to the object graph [SearchComponent].
 *
 * @see Module
 */
@Module
class ProviderModule(private val fragment: Fragment) {

    /**
     * Create a provider method binding for [SearchViewModel].
     *
     * @return Instance of view model.
     * @see Provides
     */
    @Provides
    @FeatureScope
    fun providesSearchViewModel(mSearchRepoProtocol : SearchRepoImpl) = fragment.viewModel {
        SearchViewModel(mSearchRepoProtocol)
    }

    @Provides
    fun provideDatabase(context : Context) = WeatherDatabase(context)

    @Provides
    fun provideSearchRepoProtocol(mService : ServiceProtocol, db: WeatherDatabase) = SearchRepoImpl(mService, db)

    @Provides
    fun provideGoogleRetro(gson: Gson, client: OkHttpClient): ServiceProtocol =
        Retrofit.Builder()
            .baseUrl(BuildConfig.WEATHER_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ServiceProtocol::class.java)

    @Provides
    fun provideGson(): Gson =
        GsonBuilder()
            .setLenient()
            .create()


    @Provides
    fun provideAppPreferences(application: Context, gson: Gson) =
        SharedPreferencesImpl(application, gson)


    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        return httpLoggingInterceptor.apply { httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY }
    }
    @Provides
    fun provideAuthorizationInterceptor(sharedPreferences: SharedPreferencesImpl) = AuthorizationInterceptor(sharedPreferences)

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor, authorizationInterceptor: AuthorizationInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .addInterceptor(interceptor)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()


    @Provides
    fun provideEasyLocations(): EasyLocationUtility = EasyLocationUtility(fragment.requireActivity())


}
