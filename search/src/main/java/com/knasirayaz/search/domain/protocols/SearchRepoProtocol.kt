package com.knasirayaz.search.domain.protocols

import com.google.android.gms.maps.model.LatLng
import com.knasirayaz.common.utils.Result
import com.knasirayaz.search.data.model.WeatherDataResponse
import com.knasirayaz.search.data.model.WeatherDetail

interface SearchRepoProtocol  {
    suspend fun findCityWeather(cityName: String) : WeatherDataResponse
    suspend fun findCityWeather(latLng: LatLng?) : WeatherDataResponse
    suspend fun addWeather(weatherDetail: WeatherDetail)
    suspend fun markAsFav(cityName: String, isFav : Boolean)
    suspend fun fetchWeatherDetail(cityName: String) : WeatherDetail?
    suspend fun fetchAllWeatherDetails() : List<WeatherDetail>
    suspend fun fetchFavCitiesWeatherDetails() : List<WeatherDetail>

}