package com.knasirayaz.search.domain.network

import com.knasirayaz.common.BuildConfig
import com.knasirayaz.search.data.model.WeatherDataResponse
import com.knasirayaz.search.util.AppConstants
import com.knasirayaz.weatherapp.base.BaseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ServiceProtocol {
    @GET("weather")
    suspend fun findCityWeatherData(
        @Query("lat") lat : String = "",
        @Query("lon") lng: String = "",
        @Query("q") q: String = "",
        @Query("units") units: String = AppConstants.WEATHER_UNIT,
        @Query("appid") appid: String = BuildConfig.WEATHER_API_KEY
    ): Response<WeatherDataResponse>
}