package com.knasirayaz.search.domain.repository

import com.google.android.gms.maps.model.LatLng
import com.knasirayaz.common.utils.Constants
import com.knasirayaz.search.data.local.WeatherDatabase
import com.knasirayaz.search.data.model.WeatherDataResponse
import com.knasirayaz.search.data.model.WeatherDetail
import com.knasirayaz.search.domain.network.SafeApiRequest
import com.knasirayaz.search.domain.protocols.SearchRepoProtocol
import com.knasirayaz.search.domain.network.ServiceProtocol
import javax.inject.Inject
import javax.inject.Named

class SearchRepoImpl  @Inject constructor(@Named(Constants.WeatherApi )  private val mService : ServiceProtocol,   private val db: WeatherDatabase)  : SearchRepoProtocol,
    SafeApiRequest() {

    override suspend fun findCityWeather(cityName: String): WeatherDataResponse = apiRequest {
        mService.findCityWeatherData(q = cityName)
    }

    override suspend fun findCityWeather(latLng: LatLng?): WeatherDataResponse = apiRequest {
        mService.findCityWeatherData(lat = latLng?.latitude.toString(), lng = latLng?.longitude.toString())
    }

    override suspend fun addWeather(weatherDetail: WeatherDetail) {
        db.getWeatherDao().addWeather(weatherDetail)
    }

    override suspend fun markAsFav(cityName: String, isFav : Boolean) {
        db.getWeatherDao().markAsFavourite(cityName, isFav)
    }

    override suspend fun fetchWeatherDetail(cityName: String): WeatherDetail =
        db.getWeatherDao().fetchWeatherByCity(cityName)

    override suspend fun fetchAllWeatherDetails(): List<WeatherDetail> =
        db.getWeatherDao().fetchAllWeatherDetails()

    override suspend fun fetchFavCitiesWeatherDetails(): List<WeatherDetail> =
        db.getWeatherDao().fetchAllFavWeatherDetails()

}