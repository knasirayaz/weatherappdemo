package com.knasirayaz.search.presentation.ui

import android.location.Location
import androidx.lifecycle.*
import com.dmgdesignuk.locationutils.easylocationutility.EasyLocationUtility
import com.dmgdesignuk.locationutils.easylocationutility.LocationRequestCallback
import com.google.android.gms.maps.model.LatLng
import com.knasirayaz.common.utils.Result
import com.knasirayaz.search.data.model.WeatherDataResponse
import com.knasirayaz.search.data.model.WeatherDetail
import com.knasirayaz.search.domain.protocols.SearchRepoProtocol
import com.knasirayaz.search.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchViewModel  @Inject constructor(private val mSearchRepoProtocol : SearchRepoProtocol) : ViewModel(){



    private val _weatherLiveData = MutableLiveData<Event<State<WeatherDetail>>>()
    val weatherLiveData: LiveData<Event<State<WeatherDetail>>>
        get() = _weatherLiveData

    private val _weatherDetailListLiveData = MutableLiveData<Event<State<List<WeatherDetail>>>>()
    val weatherDetailListLiveData: LiveData<Event<State<List<WeatherDetail>>>>
        get() = _weatherDetailListLiveData

    private lateinit var weatherResponse: WeatherDataResponse

    var cityName : String? = null

    fun findCityWeather(cityName: String? = null, latLng : LatLng? = null) {
        _weatherLiveData.postValue(Event(State.loading()))
        viewModelScope.launch(Dispatchers.IO) {
            try {
                weatherResponse = if(cityName == null){
                    mSearchRepoProtocol.findCityWeather(latLng)
                }else{
                    mSearchRepoProtocol.findCityWeather(cityName)
                }
                addWeatherDetailIntoDb(false, weatherResponse)
                withContext(Dispatchers.Main) {
                    val weatherDetail = WeatherDetail()
                    weatherDetail.icon = weatherResponse.weather.first().icon
                    weatherDetail.cityName = weatherResponse.name
                    weatherDetail.countryName = weatherResponse.sys.country
                    weatherDetail.temp = weatherResponse.main.temp
                    _weatherLiveData.postValue(
                        Event(
                            State.success(
                                weatherDetail
                            )
                        )
                    )
                }
            } catch (e: ApiException) {
                withContext(Dispatchers.Main) {
                    _weatherLiveData.postValue(Event(State.error(e.message ?: "")))
                }
            } catch (e: NoInternetException) {
                withContext(Dispatchers.Main) {
                    _weatherLiveData.postValue(Event(State.error(e.message ?: "")))
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    _weatherLiveData.postValue(
                        Event(
                            State.error(
                                e.message ?: ""
                            )
                        )
                    )
                }
            }
        }
    }

    private suspend fun addWeatherDetailIntoDb(isFav : Boolean, weatherResponse: WeatherDataResponse) {
        val weatherDetail = WeatherDetail()
        weatherDetail.id = weatherResponse.id
        weatherDetail.icon = weatherResponse.weather.first().icon
        weatherDetail.cityName = weatherResponse.name.toLowerCase()
        weatherDetail.countryName = weatherResponse.sys.country
        weatherDetail.temp = weatherResponse.main.temp
        weatherDetail.dateTime = AppUtils.getCurrentDateTime(AppConstants.DATE_FORMAT_1)
        weatherDetail.isFavourite = isFav

        mSearchRepoProtocol.addWeather(weatherDetail)
    }

    fun fetchWeatherDetailFromDb(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val weatherDetail = mSearchRepoProtocol.fetchWeatherDetail(cityName.toLowerCase())
            withContext(Dispatchers.Main) {
                if (weatherDetail != null) {
                    // Return true of current date and time is greater then the saved date and time of weather searched
                    if (AppUtils.isTimeExpired(weatherDetail.dateTime)) {
                        findCityWeather(cityName)
                    } else {
                        _weatherLiveData.postValue(
                            Event(
                                State.success(
                                    weatherDetail
                                )
                            )
                        )
                    }

                } else {
                    findCityWeather(cityName)
                }

            }
        }
    }

    fun fetchAllWeatherDetailsFromDb() {
        viewModelScope.launch(Dispatchers.IO) {
            val weatherDetailList = mSearchRepoProtocol.fetchAllWeatherDetails()
            withContext(Dispatchers.Main) {
                _weatherDetailListLiveData.postValue(
                    Event(
                        State.success(weatherDetailList)
                    )
                )
            }
        }
    }

    fun markAsFav(isFav : Boolean,cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            mSearchRepoProtocol.markAsFav(cityName, isFav)
        }
    }

    fun getLastLocation(locationUtility: EasyLocationUtility, callback: LocationRequestCallback) {
        if (locationUtility.permissionIsGranted()) {
            locationUtility.checkDeviceSettings(EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION)
            locationUtility.getLastKnownLocation(object : LocationRequestCallback {
                override fun onLocationResult(location: Location?) {
                    callback.onLocationResult(location)
                }

                override fun onFailedRequest(p0: String?) {
                    callback.onFailedRequest(p0)
                }
            })

        } else {
            // Permission not granted, ask for it
            locationUtility.requestPermission(EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION)
        }
    }

}