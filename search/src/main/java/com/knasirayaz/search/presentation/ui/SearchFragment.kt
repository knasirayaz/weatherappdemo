package com.knasirayaz.search.presentation.ui

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import com.dmgdesignuk.locationutils.easylocationutility.EasyLocationUtility
import com.dmgdesignuk.locationutils.easylocationutility.LocationRequestCallback
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.search.R
import com.knasirayaz.search.data.model.WeatherDetail
import com.knasirayaz.search.databinding.FragmentSearchBinding
import com.knasirayaz.search.di.DaggerSearchComponent
import com.knasirayaz.search.di.ProviderModule
import com.knasirayaz.search.util.*
import com.knasirayaz.weatherapp.MyApplication
import com.knasirayaz.weatherapp.base.BaseFragment
import javax.inject.Inject


class SearchFragment : BaseFragment<FragmentSearchBinding, SearchViewModel>(R.layout.fragment_search) {

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    @Inject
    lateinit var easyLocationUtility: EasyLocationUtility
    @Inject
    lateinit var appPreferences: SharedPreferencesImpl

    var weatherDetail : WeatherDetail? = null
    var selectedCityName : String? = null

    override fun onInitDependencyInjection() {
        DaggerSearchComponent
            .builder()
            .coreComponent(MyApplication.coreComponent(requireContext()))
            .providerModule(ProviderModule(this))
            .build()
            .inject(this)
    }

    override fun onInitDataBinding() {
        viewBinding.viewModel = viewModel
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedCityName = appPreferences.getString("cityName")

        setupUI()
        observeAPICall()
        addFav()

        if(selectedCityName.isNullOrEmpty())
        getUserLocation()
        else
            viewModel.findCityWeather(cityName = selectedCityName)


        viewBinding.ivMenu.setOnClickListener {
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://com.knasirayaz.search/viewCityFragment".toUri())
                .build()
            findNavController().navigate(request)
        }
    }

    private fun getUserLocation() {
        viewModel.getLastLocation(easyLocationUtility, object : LocationRequestCallback{
            override fun onLocationResult(location: Location?) {
                if (AppUtils.isTimeExpired(weatherDetail?.dateTime) || weatherDetail == null) {
                    viewModel.findCityWeather(latLng = LatLng(location?.latitude!!,
                        location.longitude
                    ))
                }else{
                    if(weatherDetail != null)
                    setWeatherDetails(weatherDetail!!)
                }
            }

            override fun onFailedRequest(p0: String?) {}

        })

    }

    private fun addFav() {
        viewBinding.addFav.setOnClickListener {
            weatherDetail?.cityName?.let { it1 -> viewModel.markAsFav(isFavourite(), it1) }
        }
    }



    private fun setupUI() {
        viewBinding.inputFindCityWeather.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { viewModel.fetchWeatherDetailFromDb(it) }
                viewModel.fetchAllWeatherDetailsFromDb()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean { return false}

        })

    }

    @SuppressLint("SetTextI18n")
    private fun observeAPICall() {
        viewModel.weatherLiveData.observe(viewLifecycleOwner, EventObserver { state ->
            when (state) {
                is State.Loading -> {
                }
                is State.Success -> {
                    viewBinding.textLabelSearchForCity.hide()
                    viewBinding.imageCity.hide()
                    viewBinding.constraintLayoutShowingTemp.show()
                    viewBinding.inputFindCityWeather.setQuery("", false)
                    viewBinding.inputFindCityWeather.clearFocus()
                    state.data.let { weatherDetail ->
                        setWeatherDetails(weatherDetail)
                    }

                }
                is State.Error -> {
                    //showToast(state.message)
                }
            }
        })

    }

    private fun setWeatherDetails(weatherDetail: WeatherDetail) {
        viewBinding.constraintLayoutShowingTemp.show()
        isFavourite()
        val iconCode = weatherDetail.icon?.replace("n", "d")
        AppUtils.setGlideImage(
            viewBinding.imageWeatherSymbol,
            AppConstants.WEATHER_API_IMAGE_ENDPOINT + "${iconCode}@4x.png"
        )
        changeBgAccToTemp(iconCode)
        viewBinding.textTodaysDate.text =
            AppUtils.getCurrentDateTime(AppConstants.DATE_FORMAT)
        viewBinding.textTemperature.text = weatherDetail.temp.toString()
        viewBinding.textCityName.text = "${weatherDetail.cityName?.capitalize()}, ${weatherDetail.countryName}"
        this.weatherDetail = weatherDetail
    }

    private fun isFavourite(): Boolean {
        return if(weatherDetail?.isFavourite == false) {
            viewBinding.addFav.setImageResource(R.drawable.ic_fav)
            false
        }else{
            viewBinding.addFav.setImageResource(R.drawable.ic_un_fav)
            true
        }
    }

    private fun changeBgAccToTemp(iconCode: String?) {
        when (iconCode) {
            "01d", "02d", "03d" -> viewBinding.imageWeatherHumanReaction.setImageResource(R.drawable.sunny_day)
            "04d", "09d", "10d", "11d" -> viewBinding.imageWeatherHumanReaction.setImageResource(R.drawable.raining)
            "13d", "50d" -> viewBinding.imageWeatherHumanReaction.setImageResource(R.drawable.snowfalling)
        }
    }
}