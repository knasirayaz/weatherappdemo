package com.knasirayaz.search.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.knasirayaz.common.utils.ItemClickSupport
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.search.R
import com.knasirayaz.search.databinding.FragmentViewFavCitiesBinding
import com.knasirayaz.search.di.DaggerSearchComponent
import com.knasirayaz.search.di.ProviderModule
import com.knasirayaz.search.presentation.ui.adapters.CustomAdapterSearchedCityTemperature
import com.knasirayaz.search.util.*
import com.knasirayaz.weatherapp.MyApplication
import com.knasirayaz.weatherapp.base.BaseFragment
import javax.inject.Inject

class ViewCityFragment : BaseFragment<FragmentViewFavCitiesBinding, SearchViewModel>(R.layout.fragment_view_fav_cities) {

    private lateinit var customAdapterSearchedCityTemperature: CustomAdapterSearchedCityTemperature
    @Inject
    lateinit var appPreferences: SharedPreferencesImpl

    override fun onInitDependencyInjection() {
        DaggerSearchComponent
            .builder()
            .coreComponent(MyApplication.coreComponent(requireContext()))
            .providerModule(ProviderModule(this))
            .build()
            .inject(this)
    }

    override fun onInitDataBinding() {
        viewBinding.viewModel = viewModel
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeRecyclerView()
        observeAPICall()
        viewModel.fetchAllWeatherDetailsFromDb()


        ItemClickSupport.addTo(viewBinding.recyclerViewSearchedCityTemperature).setOnItemClickListener(object : ItemClickSupport.OnItemClickListener{
            override fun onItemClicked(recyclerView: RecyclerView?, position: Int, v: View?) {
                appPreferences.putString("cityName",
                    customAdapterSearchedCityTemperature.getData(position).cityName.toString()
                )
                findNavController().navigateUp()
            }

        })
    }



    private fun initializeRecyclerView() {
        customAdapterSearchedCityTemperature = CustomAdapterSearchedCityTemperature()
        val mLayoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
        viewBinding.recyclerViewSearchedCityTemperature.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = customAdapterSearchedCityTemperature
        }
    }

    private fun observeAPICall() {
        viewModel.weatherDetailListLiveData.observe(viewLifecycleOwner, EventObserver { state ->
            when (state) {
                is State.Loading -> {
                }
                is State.Success -> {
                    if (state.data.isEmpty()) {
                        viewBinding.recyclerViewSearchedCityTemperature.hide()
                    } else {
                        viewBinding.recyclerViewSearchedCityTemperature.show()
                        customAdapterSearchedCityTemperature.setData(state.data)
                    }
                }
                is State.Error -> {
                    requireContext().showToast(state.message)
                }
            }
        })
    }


}