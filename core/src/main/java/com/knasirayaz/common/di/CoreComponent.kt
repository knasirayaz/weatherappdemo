package com.knasirayaz.common.di

import android.content.Context
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        NetworkModule::class,
        ContextModule::class,
        UtilsModule::class
    ]
)

interface CoreComponent {
    fun context(): Context
}
