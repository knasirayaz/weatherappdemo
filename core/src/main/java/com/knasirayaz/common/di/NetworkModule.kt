package com.knasirayaz.common.di

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.knasirayaz.common.BuildConfig
import com.knasirayaz.common.protocol.SharedPreferencesProtocol
import com.knasirayaz.common.scopes.AppScope
import com.knasirayaz.common.utils.AuthorizationInterceptor
import com.knasirayaz.common.utils.Constants.GoogleApi
import com.knasirayaz.common.utils.Constants.WeatherApi
import com.knasirayaz.common.utils.SharedPreferencesImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import javax.net.ssl.*

@Module
class NetworkModule {

    @Provides
    fun provideGson(): Gson =
        GsonBuilder()
            .setLenient()
            .create()


    @Provides
    fun provideAppPreferences(application: Application, gson: Gson) =
        SharedPreferencesImpl(application, gson)


    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        return httpLoggingInterceptor.apply { httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY }
    }
    @Provides
    fun provideAuthorizationInterceptor(sharedPreferences: SharedPreferencesImpl) = AuthorizationInterceptor(sharedPreferences)

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor, authorizationInterceptor: AuthorizationInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .addInterceptor(interceptor)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

    @Provides
    @Named(GoogleApi)
    fun provideGoogleRetro(gson: Gson, client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.GOOGLE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @Provides
    @Named(WeatherApi)
    fun provideWeatherRetro(gson: Gson, client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.WEATHER_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

}