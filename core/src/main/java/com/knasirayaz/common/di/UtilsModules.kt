package com.knasirayaz.common.di

import com.knasirayaz.common.protocol.SharedPreferencesProtocol
import com.knasirayaz.common.utils.SharedPreferencesImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class UtilsModule {
    @Binds
    abstract fun bindSharedPreferences(sharedPreferencesImpl: SharedPreferencesImpl): SharedPreferencesProtocol

}
