package com.knasirayaz.common.utils

import android.annotation.SuppressLint
import android.util.Log
import com.knasirayaz.common.protocol.SharedPreferencesProtocol
import okhttp3.Interceptor
import okhttp3.Response
import org.greenrobot.eventbus.EventBus

class AuthorizationInterceptor(private val sharedPreferences: SharedPreferencesImpl) : Interceptor {

    @SuppressLint("LogNotTimber")
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val token = sharedPreferences.getString(Constants.API_KEY)
        Log.d("okhttp.OkHttpClient", token.toString())
        if(request.header("No-Authentication") != null){
            request = request.newBuilder()
                .header("API-Key"," $token")
                .build()
        }

        val response = chain.proceed(request)
        if(response.code == 401){
            EventBus.getDefault().post(UnauthorizedEvent().instance())
        }

        return response
    }
}


