/*package com.knasirayaz.common.utils

import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.knasirayaz.mohapcovid.view.interfaces.IButtonEvents
import com.knasirayaz.common.utils.ToolBarSetup.Const.TOOLBAR_HIDE_BOTH_BUTTONS
import com.knasirayaz.common.utils.ToolBarSetup.Const.TOOLBAR_HIDE_LEFT_SHOW_RIGHT
import com.knasirayaz.common.utils.ToolBarSetup.Const.TOOLBAR_SHOW_BOTH_BUTTONS
import com.knasirayaz.common.utils.ToolBarSetup.Const.TOOLBAR_SHOW_LEFT_HIDE_RIGHT
import kotlinx.android.synthetic.main.toolbar.view.*


class ToolBarSetup {

    object Const{
        var TOOLBAR_SHOW_BOTH_BUTTONS = 0
        var TOOLBAR_HIDE_LEFT_SHOW_RIGHT = 1
        var TOOLBAR_SHOW_LEFT_HIDE_RIGHT = 2
        var TOOLBAR_HIDE_BOTH_BUTTONS = 3
    }


    private var toolbar: Toolbar? = null
    private var rightShadow: View? = null
    private var leftShadow: View? = null
    private var shareShadow: View? = null
    private var rightButton: ImageButton? = null
    private var leftButton: ImageButton? = null
    private var shareButton: ImageButton? = null
    private var rightText: TextView? = null
    private var leftText: TextView? = null
    private var title: TextView? = null
    private var context: Context? = null
    private var rlRight: RelativeLayout? = null
    private var rlLeft: RelativeLayout? = null
    private var rlSearch: RelativeLayout? = null
    var shareTitle: String? = null
    var shareBody: String? = null

    constructor(toolbar: Toolbar, context: Context) {
        setViews(toolbar, context)
        toolbar.visibility = View.VISIBLE
        hideRightText()
        hideShareButton()
    }

    constructor() {

    }

    private fun setViews(toolbar: Toolbar, context: Context) {
        this.toolbar = toolbar
        this.context = context
        rightButton = toolbar.btn_right //this.toolbar!!.findViewById(R.id.btn_right)
        leftButton =    toolbar .btn_left//        this.toolbar!!.findViewById(R.id.btn_left)
        rightShadow =   toolbar .rightshadow//        this.toolbar!!.findViewById(R.id.rightshadow)
        leftShadow =    toolbar .leftshadow//        this.toolbar!!.findViewById(R.id.leftshadow)
        shareShadow =   toolbar .rightshadowshare//        this.toolbar!!.findViewById(R.id.rightshadowshare)
        rightText =     toolbar .righttext//        this.toolbar!!.findViewById(R.id.righttext)
        leftText =      toolbar .lefttext//        this.toolbar!!.findViewById(R.id.lefttext)
        title =         toolbar .toolbar_title//        this.toolbar!!.findViewById(R.id.toolbar_title)
        rlRight =       toolbar .rl_right//        this.toolbar!!.findViewById(R.id.rl_right)
        rlLeft =        toolbar .rl_left//        this.toolbar!!.findViewById(R.id.rl_left)
        rlSearch =      toolbar .rl_share//        this.toolbar!!.findViewById(R.id.rl_share)
        shareButton =   toolbar .btn_share//        this.toolbar!!.findViewById(R.id.btn_share)
    }

    private fun hideShareButton() {
        if (rlSearch != null) rlSearch!!.visibility = View.GONE
    }

    fun hideToolbar() {
        toolbar!!.setVisibility(View.GONE)
    }

    fun setTitleCapsSmall() {
        title!!.isAllCaps = false
    }

    fun hideLeftButton() {
        rlLeft!!.visibility = View.GONE
        leftButton!!.visibility = View.GONE
    }

    val toolBarTitle: String
        get() = title!!.text.toString()

    fun hideRightButton() {
        rightButton!!.visibility = View.GONE
        rlRight!!.visibility = View.GONE
    }

    fun hideRightText() {
        rightText!!.visibility = View.GONE
    }

    fun disableRightText() {
        rightText!!.isEnabled = false
        rightText!!.setTextColor(ContextCompat.getColor(context!!, R.color.tertiary_text_light))
    }

    fun setAlphaOnRightBtn(value: Float) {
        rightButton!!.alpha = value
    }

    fun showLeftButton() {
        leftButton!!.visibility = View.VISIBLE
        rlLeft!!.visibility = View.VISIBLE
    }

    fun showRightButton(): ToolBarSetup {
        rightButton!!.visibility = View.VISIBLE
        rlRight!!.visibility = View.VISIBLE
        return this
    }

    fun setTitle(title: String?) {
        this.title!!.text = title
        this.title!!.visibility = View.VISIBLE
    }

    fun setTitleColor(color: Int) {
        title!!.setTextColor(ContextCompat.getColor(context!!, color))
    }

    fun setLeftButtonDrawable(id: Int) {
        leftButton!!.setImageResource(id)
    }

    fun setRightButtonDrawable(id: Int) {
        rightButton!!.setImageResource(id)
    }

    fun setLeftTitle(title: String?) {
        leftText!!.text = title
        leftButton!!.visibility = View.GONE
        leftText!!.visibility = View.VISIBLE
    }

    fun setRightTitle(title: String?) {
        rightText!!.isEnabled = true
        rightText!!.text = title
        rightText!!.visibility = View.VISIBLE
        rightButton!!.visibility = View.GONE
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnTouchListener(buttonEvents: IButtonEvents) {
        if (rightButton != null) rightButton!!.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) rightShadow!!.animate().alpha(1f).setDuration(
                100
            ).setInterpolator(DecelerateInterpolator()) else if (event.action == MotionEvent.ACTION_UP) {
                rightShadow!!.animate().alpha(0f).setDuration(100)
                    .setInterpolator(DecelerateInterpolator())
                buttonEvents.rightEvent()
            }
            true
        }
        if (leftButton != null) leftButton!!.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) leftShadow!!.animate().alpha(1f).setDuration(
                100
            ).setInterpolator(DecelerateInterpolator()) else if (event.action == MotionEvent.ACTION_UP) {
                leftShadow!!.animate().alpha(0f).setDuration(100)
                    .setInterpolator(DecelerateInterpolator())
                buttonEvents.leftEvent()
            }
            true
        }
        if (shareButton != null) shareButton!!.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) shareShadow!!.animate().alpha(1f).setDuration(
                100
            ).setInterpolator(DecelerateInterpolator()) else if (event.action == MotionEvent.ACTION_UP) {
                shareShadow!!.animate().alpha(0f).setDuration(100)
                    .setInterpolator(DecelerateInterpolator())
                buttonEvents.rightEvent()
            }
            true
        }
    }

    private fun setTextOnClickListener(buttonEvents: IButtonEvents) {
        rightText!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonEvents.rightEvent()
            }

        })
        leftText!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonEvents.leftEvent()
            }
        })
    }

    fun setBackgroudColor(colorResId: Int) {
        toolbar!!.setBackgroundColor(ContextCompat.getColor(context!!, colorResId))
    }

    private fun changeRightTextClick(isEnabled: Boolean) {
        rightText!!.setAlpha(if (isEnabled) 1f else 0.5f)
        rightText!!.isEnabled = isEnabled
    }
    fun setToolBarDefault(
        activity: Activity?,
        toolBar: Toolbar?,
        title: String?,
        mType: Int,
        buttons: IButtonEvents?
    ): ToolBarSetup? {
        val toolBarSetup = ToolBarSetup(toolBar!!, activity!!)

        if (mType == TOOLBAR_HIDE_BOTH_BUTTONS)
            toolBarSetup.setTitle(title)
        else
            toolBarSetup.setLeftTitle(title)

        toolBarSetup.setOnTouchListener(buttons!!)
        if (mType == TOOLBAR_SHOW_BOTH_BUTTONS) {
            toolBarSetup.showLeftButton()
            toolBarSetup.showRightButton()
        } else if (mType == TOOLBAR_HIDE_LEFT_SHOW_RIGHT) {
            toolBarSetup.hideLeftButton()
            toolBarSetup.showRightButton()
        } else if (mType == TOOLBAR_SHOW_LEFT_HIDE_RIGHT) {
            toolBarSetup.showLeftButton()
            toolBarSetup.hideRightButton()
        } else if (mType == TOOLBAR_HIDE_BOTH_BUTTONS) {
            toolBarSetup.hideLeftButton()
            toolBarSetup.hideRightButton()
        }
        return toolBarSetup
    }


    private fun showShareButton() {
        rlSearch!!.visibility = View.VISIBLE
    }

    companion object {
        fun setToolBarDefault(
            activity: Activity, toolBar: Toolbar,
            title: String?, events: IButtonEvents, drawableRight: Int
        ): ToolBarSetup {
            val toolBarSetup = ToolBarSetup(toolBar, activity)
            toolBarSetup.showLeftButton()
            toolBarSetup.showRightButton()
            toolBarSetup.setOnTouchListener(events)
            toolBarSetup.setTitle(title)
            toolBarSetup.hideLeftButton()
            toolBarSetup.setTextOnClickListener(events)
            toolBarSetup.changeRightTextClick(true)
            toolBarSetup.setRightButtonDrawable(drawableRight)
            return toolBarSetup
        }
    }

}*/