package com.knasirayaz.common.utils

object Constants {
    const val IS_FIRST_TIME = "isFirstTime"
    const val PERSIST_LOCATION = "PersistLocation"
    const val SHARE_DATA_STATE = "ShareDataState"
    const val PHONE_NUMBER = "PhoneNumber"

    const val URL = "url"
    const val API_KEY = "api_key"

    const val GoogleApi = "GoogleApi"
    const val WeatherApi = "WeatherApi"


}