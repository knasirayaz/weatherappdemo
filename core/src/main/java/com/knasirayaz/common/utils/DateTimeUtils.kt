package com.knasirayaz.mohapcovid.view.utils

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    //2020-04-03 12:20:15.127

    var DD_MM_YY_HH_MM= "yyyy-MM-dd hh:mm:ss"
    var DD_MM_YY_HH_MM_A = "dd/MM/yyy hh:mm a"
    var EVENT_VIEW = "MMMM yyyy"
    var OFFER_VIEW = "dd MMMM yyyy"
    var REVIEW_DATE_FORMAT = "dd/MM/yyyy"
    var REVIEW_VIEW = "dd MMMM yyyy"
    var DAY_FORMAT = "EEE"
    var DAY_FORMAT_VIEW = "EEEE"
    var DAY_TIME = "hh:mm:ss"
    var DAY_TIME_VIEW = "hh:mm"
    var MM_DD_YYYY = "MMM dd, yyyy hh:mm a"
    var SERVER_DATE = "yyyy-MM-dd'T'HH:mm:ss"

    fun getDateString(
        date: Date?,
        format: String?,
        lang: String?
    ): String { //Nov 15, 2019
        val sdf = SimpleDateFormat(format,
            if (lang.equals("ar")){
                Locale("ar", "UAE")
            } else if (lang.equals("en")) {
                Locale("en", "UK")
            }else{
                Locale("ur", "PK")
            }
        )
        return if (date != null) sdf.format(date) else ""
    }

    fun getMonthDayYear(timestamp: String?, arabic: String?): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //2020-03-25T19:08:00
        var convertedDate: Date? = null
        try {
            convertedDate = dateFormat.parse(timestamp!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return getDateString(convertedDate, MM_DD_YYYY, arabic)
    }


    fun convertDate(formatFrom: String?,timestamp: String?,formatTo: String?, lang: String?): String {
        val dateFormat = SimpleDateFormat(formatFrom)
        var convertedDate: Date? = null
        try {
            convertedDate = dateFormat.parse(timestamp!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return getDateString(convertedDate, formatTo, lang)
    }

    fun getDateString(
        dateLng: Long?,
        isUNIX: Boolean,
        format: String?
    ): String? {
        if (dateLng == null) return null
        val date = Date(if (isUNIX) dateLng * 1000 else dateLng)
        val sdf =
            SimpleDateFormat(format, Locale.UK)
        sdf.timeZone = TimeZone.getDefault()
        return sdf.format(date)
    }

    fun getDateFromString(date: String?, format: String?): Date? {
        val sdf =
            SimpleDateFormat(format, Locale.getDefault())
        val dateObj: Date
        try {
            dateObj = sdf.parse(date)
            return dateObj
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    fun getDateLongFromString(
        date: String?,
        format: String?,
        isUNIX: Boolean
    ): Long {
        val sdf =
            SimpleDateFormat(format, Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val dateObj: Date
        try {
            dateObj = sdf.parse(date)
            return if (isUNIX) dateObj.time / 1000 else dateObj.time
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return 0
    }

    fun getYearMonth(addYear: Int): String {
        val c = Calendar.getInstance()
        var year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        year += addYear
        return "$month/$year"
    }

    fun getCurrentTime(): Long {
        val c = Calendar.getInstance()
        return c.timeInMillis
    }
    fun getTomorrowTime(): Long {
        val c = Calendar.getInstance()
        c.add(Calendar.DATE,  1)
        return c.timeInMillis
    }

    fun getTimeInMilis(date : String) : Long{
        var sdf = SimpleDateFormat(DD_MM_YY_HH_MM)
        var date = sdf.parse(date)
        return date.time
    }

    fun getTimeInMilis(date : String, format:String?) : Long{
        try {
            var sdf = SimpleDateFormat(format)
            var date = sdf.parse(date)
            return date.time
        }catch (e : java.lang.Exception){
            Log.e("","")
        }
        return 0L
    }


    fun getWhatDateIsToday (): String? {
        return getDateString(getCurrentTime(), false , REVIEW_DATE_FORMAT)
    }

    fun getWhatDateIsTomorrow (): String? {
        return getDateString(getTomorrowTime(), false , REVIEW_DATE_FORMAT)
    }


    fun getMonthNameFromFormattedDate(date : String?, format:String?) : String {
        val month_date = SimpleDateFormat("MMMM")
        return month_date.format(getTimeInMilis(date.toString(),  format))
    }

    fun getDayFromFormattedDate(date : String?, format:String?) : String {
        val month_date = SimpleDateFormat("dd")
        return month_date.format(getTimeInMilis(date.toString(),  format))
    }
    fun getDayNameFromFormattedDate(date : String?, format:String?) : String {
        val month_date = SimpleDateFormat("EEEE")
        return month_date.format(getTimeInMilis(date.toString(),  format))
    }
}

