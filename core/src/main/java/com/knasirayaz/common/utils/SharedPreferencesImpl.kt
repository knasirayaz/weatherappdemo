package com.knasirayaz.common.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.knasirayaz.common.protocol.SharedPreferencesProtocol

class SharedPreferencesImpl (context: Context, val gson: Gson) : SharedPreferencesProtocol {


    private var prefs: SharedPreferences = context.getSharedPreferences("weatherapp", Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = prefs.edit()


    fun contains(key: String) = prefs.contains(key)

     fun putString(key: String, value: String) {
        editor.putString(key, value).commit()
    }

     fun putBoolean(key: String, value: Boolean?) {
        editor.putBoolean(key, value!!).commit()
    }

     fun putInt(key: String, value: Int) {
        editor.putInt(key, value).commit()
    }

     fun putFloat(key: String, value: Float?) {
        editor.putFloat(key, value!!).commit()
    }

     fun putLong(key: String, value: Long) {
        editor.putLong(key, value).commit()
    }

     fun putList(key: String, list: List<*>) {
        val json = gson.toJson(list)
        putString(key,json)
    }

     fun getString(key: String): String? {
        return prefs.getString(key, null)
    }

     fun getInt(badgeCounter: String, key: String): Int {
        return prefs.getInt(key, 0)
    }

     fun getBoolean(key: String): Boolean? {
        return prefs.getBoolean(key, false)
    }

     fun getFloat(key: String): Float {
        return prefs.getFloat(key, 0.0f)
    }

     fun getString(key: String, defValue: String): String? {
        return prefs.getString(key, defValue)
    }

    fun <T> getList(key: String): T {
        val json = getString(key,"")
        return gson.fromJson(json, object: TypeToken<T>(){}.type)
    }

     fun removeString(key: String) {
        editor.remove(key).commit()
    }

     fun putString(key: String, obj: Any) {
        editor.putString(key, gson.toJson(obj))
    }

     fun <A> getObject(key: String, type: Class<A>): A {
        return gson.fromJson(getString(key), type)
    }

    override fun getSharedPreferences(application: Application, gson: Gson): SharedPreferencesImpl {
        return this
    }


}