
package com.knasirayaz.mohapcovid.view.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

inline fun <reified T : Any> Activity.launchActivity(finishActivity: Boolean = true) {
    startActivity(Intent(this, T::class.java))
    if (finishActivity) {
        finish()
    }
}

inline fun <reified T : Any> Activity.launchActivity(finishActivity: Boolean = true, bundle: Bundle) {
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    startActivity(intent)
    if (finishActivity) {
        finish()
    }
}

inline fun <reified T : Any> Activity.launchActivityAddFlag() {
    val intent = Intent(this, T::class.java)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
    finish()
}

fun Activity.launchWeb(webLink: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(webLink))
    startActivity(intent)
}

fun Context.toast(message: Any?) {
    Toast.makeText(this, "${message}", Toast.LENGTH_SHORT).show()
}

    fun snackbar(v: View, message: Any?) {
    val snackbar = Snackbar.make(v,"$message",Snackbar.LENGTH_LONG)
    val snackbarView = snackbar.view
    val textView = snackbarView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    textView.maxLines = 1
    snackbar.show()
}

inline fun <reified T> SharedPreferences.put(key: String, value: T) {
    val editor = this.edit()

    when (T::class) {
        String::class -> editor.putString(key, value as String)
        Boolean::class -> editor.putBoolean(key, value as Boolean)
        Int::class -> editor.putInt(key, value as Int)
        Float::class -> editor.putFloat(key, value as Float)
        Long::class -> editor.putLong(key, value as Long)
        else -> "Not able to perform operation"
    }
    editor.apply()
}

inline fun <reified T> SharedPreferences.get(key: String, defaultValue: T): T {
    when (T::class) {
        String::class -> return this.getString(key, defaultValue as String) as T
        Boolean::class -> return this.getBoolean(key, defaultValue as Boolean) as T
        Int::class -> return this.getInt(key, defaultValue as Int) as T
        Float::class -> return this.getFloat(key, defaultValue as Float) as T
        Long::class -> return this.getLong(key, defaultValue as Long) as T
    }
    return defaultValue
}

internal fun TextView.setTextColorRes(@ColorRes color: Int) = setTextColor(context.getColorCompat(color))
internal fun Context.getColorCompat(@ColorRes color: Int) = ContextCompat.getColor(this, color)

fun Context.themeColor(@AttrRes resId: Int): Int {
    val typedValue = TypedValue()
    theme.resolveAttribute(resId, typedValue, true)
    return typedValue.data
}

fun dateformatServer(date: Date) = SimpleDateFormat("yyyy-MM-dd",Locale("en")).format(date)

/**
 * Transforms static java function Snackbar.make() to an extension function on View.
 */
fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    Snackbar.make(this, snackbarText, timeLength).show()
}

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
fun View.setupSnackbar(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbar(context.getString(it), timeLength)
        }
    })
}

fun daysAgo(daysAgo: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)
    return calendar.time
}


fun String?.getMonthNameFromFormattedDate() : String?{
   return DateTimeUtils.getMonthNameFromFormattedDate(this, DateTimeUtils.REVIEW_DATE_FORMAT)
}
fun String?.getDayFromFormattedDate() : String?{
    return DateTimeUtils.getDayFromFormattedDate(this, DateTimeUtils.REVIEW_DATE_FORMAT)
}

fun String?.getDayNameFromFormattedDate() : String?{
    return DateTimeUtils.getDayNameFromFormattedDate(this, DateTimeUtils.REVIEW_DATE_FORMAT)
}

