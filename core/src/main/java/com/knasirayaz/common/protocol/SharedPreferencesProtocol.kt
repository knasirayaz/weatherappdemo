package com.knasirayaz.common.protocol

import android.app.Application
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.knasirayaz.common.utils.SharedPreferencesImpl

interface SharedPreferencesProtocol {
    fun getSharedPreferences(application: Application, gson: Gson) : SharedPreferencesImpl
}