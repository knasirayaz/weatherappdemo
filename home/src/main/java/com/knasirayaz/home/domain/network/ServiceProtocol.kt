package com.knasirayaz.home.domain.network

import com.knasirayaz.weatherapp.base.BaseResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface ServiceProtocol {
    @Headers("No-Authentication: true")
    @GET("api/Announcement/GetConvidCounters")
    suspend fun getWeatherByCity() : BaseResponse
}