package com.knasirayaz.home.domain.protocols

import com.knasirayaz.common.utils.Result

interface HomeRepositoryProtocol {
    suspend fun getWeatherByCity() : Result<*>
}