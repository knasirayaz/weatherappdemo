package com.knasirayaz.home.domain.repository

import com.knasirayaz.common.utils.Constants
import com.knasirayaz.common.utils.Result
import com.knasirayaz.home.domain.network.ServiceProtocol
import com.knasirayaz.home.domain.protocols.HomeRepositoryProtocol
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class HomeRepositoryImpl @Inject constructor(@Named(Constants.WeatherApi )  private val mService : ServiceProtocol) : HomeRepositoryProtocol {

    override suspend fun getWeatherByCity() = withContext(Dispatchers.IO) {
        try {
            val response = mService.getWeatherByCity()
            return@withContext Result.Success(response)
        } catch (e: Exception) {
            return@withContext Result.Error(e)
        }
    }

}