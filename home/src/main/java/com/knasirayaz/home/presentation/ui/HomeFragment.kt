package com.knasirayaz.home.presentation.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import com.knasirayaz.weatherapp.base.BaseFragment
import com.knasirayaz.home.R
import com.knasirayaz.home.databinding.FragmentHomeBinding
import com.knasirayaz.home.di.DaggerHomeComponent
import com.knasirayaz.home.di.HomeModule
import com.knasirayaz.weatherapp.MyApplication

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(R.layout.fragment_home) {


    override fun onInitDependencyInjection() {
        DaggerHomeComponent
            .builder()
            .coreComponent(MyApplication.coreComponent(requireContext()))
            .homeModule(HomeModule(this))
            .build()
            .inject(this)
    }

    override fun onInitDataBinding() {
        viewBinding.viewModel = viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("","")

    }
}