package com.knasirayaz.home.presentation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.knasirayaz.common.utils.Result
import com.knasirayaz.home.domain.protocols.HomeRepositoryProtocol
import javax.inject.Inject

class HomeViewModel  @Inject constructor(private val mHomeRepositoryProtocol : HomeRepositoryProtocol) : ViewModel(){

    fun getWeatherByCity () = liveData {
        emit(Result.Loading)
        val response = mHomeRepositoryProtocol.getWeatherByCity()
        emit(response)
    }

}