/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.knasirayaz.home.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.knasirayaz.common.BuildConfig
import com.knasirayaz.common.di.ContextModule
import com.knasirayaz.common.extentions.viewModel
import com.knasirayaz.common.scopes.FeatureScope
import com.knasirayaz.common.utils.AuthorizationInterceptor
import com.knasirayaz.common.utils.Constants
import com.knasirayaz.common.utils.SharedPreferencesImpl
import com.knasirayaz.home.domain.network.ServiceProtocol
import com.knasirayaz.home.domain.protocols.HomeRepositoryProtocol
import com.knasirayaz.home.domain.repository.HomeRepositoryImpl
import com.knasirayaz.home.presentation.ui.HomeFragment
import com.knasirayaz.home.presentation.ui.HomeViewModel
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Scope
import javax.inject.Singleton


@Module
class HomeModule(private val fragment: HomeFragment) {

    @Provides
    @FeatureScope
    fun providesHomeViewModel(mHomeRepositoryProtocol : HomeRepositoryImpl) = fragment.viewModel {
        HomeViewModel(mHomeRepositoryProtocol)
    }

    @Provides
    fun provideHomeRepoProtocol(mService : ServiceProtocol) = HomeRepositoryImpl(mService)

    @Provides
    fun provideGoogleRetro(gson: Gson, client: OkHttpClient): ServiceProtocol =
        Retrofit.Builder()
            .baseUrl(BuildConfig.GOOGLE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ServiceProtocol::class.java)

    @Provides
    fun provideGson(): Gson =
        GsonBuilder()
            .setLenient()
            .create()


    @Provides
    fun provideAppPreferences(application: Context, gson: Gson) =
        SharedPreferencesImpl(application, gson)


    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        return httpLoggingInterceptor.apply { httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY }
    }
    @Provides
    fun provideAuthorizationInterceptor(sharedPreferences: SharedPreferencesImpl) = AuthorizationInterceptor(sharedPreferences)

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor, authorizationInterceptor: AuthorizationInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .addInterceptor(interceptor)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
}
